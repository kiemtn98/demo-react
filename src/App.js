import React from "react";
import Results from './Results';

function App() {
    return (
        <div className="App">
            <Results/>
        </div>
    );
}

export default App;

import React from "react";

export const Detail = (props) => {
    return (
        <React.Fragment>
            <dt>{props.title}</dt>
            {
                props.data.map((it, index) => {
                    return (<dd key={index}>{it.name}</dd>);
                })
            }
        </React.Fragment>
    );
}
export const response = {
    code: 0,
    message: 'Get list success',
    data: {
        songs: [
            {
                'name': 'Sóng gió'
            },
            {
                'name': 'Sóng gió 1'
            },
            {
                'name': 'Sóng gió 2'
            },
            {
                'name': 'Sóng gió 3'
            },

        ],
        albums: [
            {
                'name': 'Nhạc trẻ'
            },
            {
                'name': 'Nhạc trẻ 1'
            },
            {
                'name': 'Nhạc trẻ 2'
            },
            {
                'name': 'Nhạc trẻ 3'
            },
        ],
        singers: [
            {
                'name': 'Jack'
            },
            {
                'name': 'Sơn Tùng M-TP'
            },
            {
                'name': 'Hương Ly'
            },
            {
                'name': 'Đinh Đại Vũ'
            },
        ]
    }
};
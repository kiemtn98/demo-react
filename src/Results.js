import React, {useState, useEffect} from 'react';
import {Detail} from "./Detail";
import {response} from "./response";

const Results = () => {
    const [singers, setSingers] = useState([]);
    const [album, setAlbum] = useState([]);
    const [song, setSong] = useState([]);

    useEffect(() => {
        const {data} = response;
        setSingers(data.singers);
        setSong(data.songs);
        setAlbum(data.albums);
    }, []);

    return (
        <React.Fragment>
            <dl>
                {
                    song.length !== 0 ? <Detail title={'Bài hát'} data={song}/> : ''
                }
                {
                    album.length !== 0 ? <Detail title={'Album'} data={album}/> : ''
                }
                {
                    singers.length !== 0 ? <Detail title={'Nhạc sĩ'} data={singers}/> : ''
                }
            </dl>
        </React.Fragment>
    )
};

export default Results;